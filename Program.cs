﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelSilv
{
    class Program
    {
        static void Main(string[] args)
        {
            int lastYear = 0;

            int startRow = 5;
            int startColumn = 1;

            int startYearRow = 1;
            int startYearCollumn = 2;

            int lastRow = 10143;



            String fileName = "serie_10062018174823.xlsx";

            Console.WriteLine();


            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(AppDomain.CurrentDomain.BaseDirectory+fileName);
            Excel.Worksheet xlWorksheet = xlWorkbook.Sheets[1]; // assume it is the first sheet
            Excel.Range xlRange = xlWorksheet.UsedRange; // get the entire used range
            Excel.Worksheet xlWorksheetWrite = xlWorkbook.Worksheets.Add(Type.Missing,
                        xlWorksheet, Type.Missing, Type.Missing);
            xlWorksheetWrite.Name = "Generated";


            String cellDate = xlRange.Cells[startRow, startColumn].Value.ToString();
            String cellValue = xlRange.Cells[startRow, startColumn + 1].Value.ToString();

            String[] date = cellDate.Split(' ')[0].Split('/');

            lastYear = Int32.Parse(date[2]);
            String toWriteYear = lastYear + "-" + (lastYear + 1);
            for (int day = 1; day <= 31; day++)
            {
                xlWorksheetWrite.Cells[startYearRow + day, startYearCollumn] = toWriteYear;
                xlWorksheetWrite.Cells[startYearRow + day, startYearCollumn + 1] = day;
            }



            xlWorksheetWrite.Cells[1, 1] = "Estação";
            xlWorksheetWrite.Cells[1, 2] = "Ano";
            xlWorksheetWrite.Cells[1, 3] = "Dia";
            xlWorksheetWrite.Cells[1, 4] = "Out";
            xlWorksheetWrite.Cells[1, 5] = "Escoamento";
            xlWorksheetWrite.Cells[1, 6] = "Nov";
            xlWorksheetWrite.Cells[1, 7] = "Escoamento";
            xlWorksheetWrite.Cells[1, 8] = "Dez";
            xlWorksheetWrite.Cells[1, 9] = "Escoamento";

            xlWorksheetWrite.Cells[1, 10] = "Jan";
            xlWorksheetWrite.Cells[1, 11] = "Escoamento";
            xlWorksheetWrite.Cells[1, 12] = "Fev";
            xlWorksheetWrite.Cells[1, 13] = "Escoamento";
            xlWorksheetWrite.Cells[1, 14] = "Mar";
            xlWorksheetWrite.Cells[1, 15] = "Escoamento";
            xlWorksheetWrite.Cells[1, 16] = "Abr";
            xlWorksheetWrite.Cells[1, 17] = "Escoamento";

            xlWorksheetWrite.Cells[1, 18] = "Mai";
            xlWorksheetWrite.Cells[1, 19] = "Escoamento";
            xlWorksheetWrite.Cells[1, 20] = "Jun";
            xlWorksheetWrite.Cells[1, 21] = "Escoamento";
            xlWorksheetWrite.Cells[1, 22] = "Jul";
            xlWorksheetWrite.Cells[1, 23] = "Escoamento";
            xlWorksheetWrite.Cells[1, 24] = "Ago";
            xlWorksheetWrite.Cells[1, 25] = "Escoamento";
            xlWorksheetWrite.Cells[1, 26] = "Set";
            xlWorksheetWrite.Cells[1, 27] = "Escoamento";


            for (; startRow <= lastRow; startRow++) {


                 cellDate = xlRange.Cells[startRow, startColumn].Value.ToString();
                 cellValue = xlRange.Cells[startRow, startColumn + 1].Value.ToString();

                 date = cellDate.Split(' ')[0].Split('/');

                if (lastYear < Int32.Parse(date[2]))
                {
                    startYearRow += 31;
                    lastYear = Int32.Parse(date[2]);
                    toWriteYear = lastYear + "-" + (lastYear + 1);
                    for (int day = 1; day <= 31; day++)
                    {
                        xlWorksheetWrite.Cells[startYearRow + day, startYearCollumn] = toWriteYear;
                        xlWorksheetWrite.Cells[startYearRow + day, startYearCollumn + 1] = day;
                    }

                }


                xlWorksheetWrite.Cells[startYearRow + Int32.Parse(date[0]), startYearCollumn + 2 * (monthToColumn(Int32.Parse(date[1])))] = cellValue;

            }

            xlWorkbook.Save();
            xlWorkbook.Close();
            xlApp.Quit();

            Console.WriteLine("fim");

        }





        private static int monthToColumn(int i)
        {
            switch (i)
            {
                case 10: return 1;
                case 11: return 2;
                case 12: return 3;
                case 1: return 4;
                case 2: return 5;
                case 3: return 6;
                case 4: return 7;
                case 5: return 8;
                case 6: return 9;
                case 7: return 10;
                case 8: return 11;
                case 9: return 12;

            }

            return -1;
        }

    }
}
